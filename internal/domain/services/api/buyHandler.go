package api

import (
	"context"
	"errors"
	"exchange-api/internal/domain/entity"
	"exchange-api/internal/domain/entity/database"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"log"
	"net/http"
	"strings"
	"time"
)

func getBuyToken(pair string) string {
	return strings.Split(pair, "/")[0]
}
func getSellToken(pair string) string {
	return strings.Split(pair, "/")[1]
}

var pendentTransactions = make(chan *gin.Context)

func BuyHandler(ctx *gin.Context) {
	defer ctx.Done()
	ctx.Set("pendent_transaction_ch", pendentTransactions)
	bookTransaction := entity.BookTransaction{}
	if err := ctx.ShouldBindJSON(&bookTransaction); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "error",
		})
	}
	buyTransactionCh := make(chan *entity.ConfirmedBuy)
	errorTransactionCh := make(chan error)
	ctx.Set("buy_transaction_ch", buyTransactionCh)
	ctx.Set("err_transaction_ch", errorTransactionCh)

	buyTransaction := entity.Buy{
		UUID:      uuid.New(),
		Timestamp: bookTransaction.Timestamp,
		BuyToken:  getBuyToken(bookTransaction.Pair),
		SellToken: getSellToken(bookTransaction.Pair),
		Price:     bookTransaction.Price,
		Quantity:  bookTransaction.Quantity,
	}
	timeContext, cancelTimeContext := context.WithTimeout(ctx, 5*50*time.Millisecond)
	defer cancelTimeContext()
	go sendBuyTransaction(buyTransaction, ctx)

	select {
	case transaction := <-buyTransactionCh:
		ctx.JSON(http.StatusAccepted, gin.H{
			"transaction": transaction,
		})
		ctx.Done()
	case err := <-errorTransactionCh:
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		ctx.Done()
	case <-timeContext.Done():
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "transaction not sucessed",
		})
		ctx.Done()
	}
}

func sendBuyTransaction(buy entity.Buy, ctx *gin.Context) {
	db, err := database.ConnectDB()
	if err != nil {
		errCh, _ := ctx.Get("err_transaction_ch")
		log.Println(err)
		errCh.(chan error) <- errors.New("Server error")
		return
	}
	_, err = db.Insert(buy, "buy_transactions")
	if err != nil {
		errCh, _ := ctx.Get("err_transaction_ch")
		log.Println(err)
		errCh.(chan error) <- errors.New("Server error")
		return
	}

	buyCh, _ := ctx.Get("buy_transaction_ch")
	buyCh.(chan *entity.ConfirmedBuy) <- &entity.ConfirmedBuy{
		UUID:      buy.UUID,
		Timestamp: buy.Timestamp,
		BuyToken:  buy.BuyToken,
		SellToken: buy.SellToken,
		Price:     buy.Price,
		Quantity:  buy.Quantity,
	}
}
