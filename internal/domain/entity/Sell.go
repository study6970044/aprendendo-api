package entity

type Sell struct {
	Timestamp int32
	SellToken string
	BuyToken  string
	Price     float32
	Quantity  float32
}
