package entity

type BookTransaction struct {
	Timestamp int32   `json:"timestamp"`
	Pair      string  `json:"pair"`
	Side      string  `json:"side"`
	Price     float32 `json:"price"`
	Quantity  float32 `json:"quantity"`
}
