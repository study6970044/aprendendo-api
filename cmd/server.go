package main

import (
	"exchange-api/internal/domain/services/api"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
)

func main() {
	server := gin.Default()
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	server.Use(gin.Logger())
	server.POST("/services/transactions/buy", api.BuyHandler)
	server.Run()
}
