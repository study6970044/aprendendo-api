package database

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"reflect"
	"strings"
)

var queryInsert = "INSERT INTO %s (%s) VALUES (%s);"

type DB struct {
	DBEngine *sql.DB
}

func (db DB) Insert(entity any, tableName string) (sql.Result, error) {
	insert := newInsertEntity(entity)
	return db.DBEngine.Exec(fmt.Sprintf(
		queryInsert, tableName, insert.GetColumns(), insert.GetValues(),
	),
	)
}

func ConnectDB() (*DB, error) {
	driverName := os.Getenv("DB_DRIVER_NAME")
	connStr := os.Getenv("DB_STRING_CON")

	db, err := sql.Open(driverName, connStr)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Server Error")
	}
	log.Printf("Database '%s' conected over user '%s'", os.Getenv("DB_NAME"), os.Getenv("DB_USER"))
	return &DB{
		DBEngine: db,
	}, nil
}

type InsertEntity struct {
	Columns []string
	Values  []string
}

func newInsertEntity(entity any) *InsertEntity {
	columns := make([]string, 0)
	values := make([]string, 0)
	v := reflect.ValueOf(entity)
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		columns = append(columns, strings.ToLower(t.Field(i).Name))

		switch v.Field(i).Kind() {
		case reflect.Int64, reflect.Int32, reflect.Int, reflect.Float64, reflect.Float32:
			values = append(values, fmt.Sprintf("%v", v.Field(i).Interface()))
		default:
			values = append(values, fmt.Sprintf("'%s'", v.Field(i).Interface()))
		}

	}
	return &InsertEntity{
		Columns: columns,
		Values:  values,
	}
}

func (i InsertEntity) GetColumns() string {
	columnsStr := strings.Builder{}
	for index, c := range i.Columns {
		columnsStr.WriteString(c)
		if index < len(i.Columns)-1 {
			columnsStr.WriteString(",")
		}
	}
	return columnsStr.String()
}
func (i InsertEntity) GetValues() string {
	valuesStr := strings.Builder{}
	for index, c := range i.Values {
		valuesStr.WriteString(c)
		if index < len(i.Values)-1 {
			valuesStr.WriteString(",")
		}
	}
	return valuesStr.String()
}
