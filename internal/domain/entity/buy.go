package entity

import "github.com/google/uuid"

type Buy struct {
	UUID      uuid.UUID
	Timestamp int32
	BuyToken  string
	SellToken string
	Price     float32
	Quantity  float32
}
type ConfirmedBuy struct {
	UUID      uuid.UUID
	Timestamp int32
	BuyToken  string
	SellToken string
	Price     float32
	Quantity  float32
}
